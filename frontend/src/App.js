import React from "react";
import "./App.css";
import { useEffect, useState } from "react";
import { fetchPhotos, uploadPhoto } from "./api";
import { Route, Switch } from "react-router-dom";
import Navigation from "./components/navigation/Navigation";
import "bootstrap/dist/css/bootstrap.min.css";
import Button from "react-bootstrap/Button";
import PhotoModal from "./components/photomodal/PhotoModal";

function App() {
  const [photos, setPhotos] = useState([]);
  const [selectedFile, setSelectedFile] = useState(null);
  const [uploadStatus, setUploadStatus] = useState(null);
  const [fetchStatusError, setFetchStatusError] = useState(null);

  useEffect(() => {
    async function getPhotos() {
      setPhotos(await fetchPhotos());
    }
    getPhotos();
  }, []);

  const refetchPhotos = async () => {
    setFetchStatusError(null);
    try {
      setPhotos(await fetchPhotos());
    } catch {
      setFetchStatusError("Unable to fetch photos");
    }
  };

  const submitForm = async (event) => {
    try {
      event.preventDefault();
      await uploadPhoto(selectedFile);
      await refetchPhotos();
      setUploadStatus("Success!");
    } catch {
      setUploadStatus("Unable to upload photo");
    } finally {
      setTimeout(() => {
        setUploadStatus(null);
      }, 3000);
    }
  };

  const onSelectFile = (event) => {
    setSelectedFile(event.target.files[0]);
  };

  return (
    <div className="App">
          
      <Navigation />
         
      <Switch>
        <Route exact path="/">
          {fetchStatusError && <p>{fetchStatusError}</p>} {uploadStatus && <p>{uploadStatus}</p>}                
          <form onSubmit={submitForm}>
                
            <input type="file" name="photo" onChange={onSelectFile} />
                        
            <Button className="upload" type="submit">
              Upload
            </Button>
                                  
          </form>
        </Route>
          
        <Route path="/photos">
                          
          {photos.map((photo) => {
            return <PhotoModal key={`${photo.src}-modal`} photo={photo} />;
          })}
                              
        </Route>
      </Switch>
    </div>
  );
}

export default App;
