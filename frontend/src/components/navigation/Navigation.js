import React from "react";
import { Link } from "react-router-dom";
import "./Navigation.css";

function Navigation(props) {
  return (
    <div className="links">
      <div className="home">
        <Link to="/">Home</Link>
      </div>
      <div className="photos-link">
        <Link to="/photos">Photos</Link>
      </div>
    </div>
  );
}
export default Navigation;
