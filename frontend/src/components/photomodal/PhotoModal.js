import React, { useState, useEffect } from "react";
import Card from "react-bootstrap/Card";
import Modal from "react-modal";
import { api_baseURL, photoSize } from "../../api";
import "./PhotoModal.css";

Modal.setAppElement("#root");

function PhotoModal({ photo }) {
  const [modalIsOpen, setModalIsOpen] = useState(false);
  const [size, setSize] = useState();
  const photoIdPoint = `/photos${photo.src.slice(8)}`;

  useEffect(() => {
    async function getSize() {
      setSize(await photoSize(photoIdPoint));
    }
    getSize();
  }, [photoIdPoint]);

  const mb = size / 1000000;
  const kb = mb / 1000;

  return (
    <>
      <button className="modalButton" onClick={() => setModalIsOpen(true)}>
        <img
          key={photo.src + "-button"}
          src={`${api_baseURL}${photo.src}`}
          alt={photo.src}
          width="250px"
          height="250px"
        />
                                                
      </button>
      <Modal isOpen={modalIsOpen} onRequestClose={() => setModalIsOpen(false)}>
        <Card style={{ width: "18rem" }}>
          <Card.Img key={photo.src} src={`${api_baseURL}${photo.src}`} alt={photo.src} />
          <Card.Body>
            <Card.Title>
              {"Image size: " + size + " b,"} {mb + " mb,"} {kb + " kb"}
            </Card.Title>
            <button onClick={() => setModalIsOpen(false)}>Close</button>
          </Card.Body>
        </Card>
      </Modal>
       
    </>
  );
}

export default PhotoModal;
