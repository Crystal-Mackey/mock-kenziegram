import { Component } from "react";
import { fetchPhotos, api_baseURL, uploadPhoto } from "./api";

export default class AppClassExample extends Component {
  state = {
    photos: [],
    selectedFile: null,
    uploadStatus: null,
    fetchStatusError: null,
  };

  componentDidMount() {
    this.fetchPhotos();
  }

  async fetchPhotos() {
    try {
      this.setState(() => ({ fetchStatusError: null }));
      const photos = await fetchPhotos();
      this.setState(() => ({ photos }));
    } catch {
      this.setState(() => ({ fetchStatusError: "Unable to fetch photos" }));
    }
  }

  submitForm = async (event) => {
    try {
      event.preventDefault();
      const { selectedFile } = this.state;
      await uploadPhoto(selectedFile);
      await this.fetchPhotos();
      this.setState(() => ({ uploadStatus: "Success!" }));
    } catch {
      this.setState(() => ({ uploadStatus: "Unable to upload photo" }));
    } finally {
      setTimeout(() => {
        this.setState(() => ({ uploadStatus: null }));
      }, 3000);
    }
  };

  onSelectFile = (event) => {
    this.setState(() => ({ selectedFile: event.target.files[0] }));
  };

  render() {
    const { uploadStatus, photos, fetchStatusError } = this.state;
    return (
      <div className="App">
        {fetchStatusError && <p>{fetchStatusError}</p>}
        {uploadStatus && <p>{uploadStatus}</p>}
        <form onSubmit={this.submitForm}>
          <input type="file" name="photo" onChange={this.onSelectFile} />
          <button type="submit">Submit</button>
        </form>
        {photos.map((photo) => (
          <img key={photo} src={`${api_baseURL}${photo}`} alt={photo} width={"150px"} height={"150px"} />
        ))}
      </div>
    );
  }
}
