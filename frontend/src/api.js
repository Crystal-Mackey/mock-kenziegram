import axios from "axios";
export const api_baseURL = "http://localhost:5000";

const axiosInstance = axios.create({
  baseURL: api_baseURL,
});
export default axiosInstance;
export const fetchPhotos = async () => {
  const { data } = await axiosInstance.get("/photos", {});
  return data;
};
export const uploadPhoto = async (data) => {
  const formData = new FormData();
  formData.append("photo", data);
  await axiosInstance.post("/uploads", formData);
};

export const photoSize = async (photoId) => {
  const response = await axiosInstance.get(`${photoId}`, {});
  const data = response.data;
  return data.size;
};
