import express, { urlencoded } from "express";
import cors from "cors";
import { uploader, uploadDirectory, getUploadedFiles, findUploadedFile } from "./utils";
import path from "path";

const app = express();
const port = 5000;
const multer = require("multer");
const storage = multer.diskStorage({
  destination: "./uploads",
  filename: function (req, file, cb) {
    cb(null, file.fieldname + "-" + path.extname(file.originalname));
  },
});

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.set("view engine", "ejs");
app.use("/uploads", express.static(uploadDirectory));

app.get("/", async (req, res) => {
  res.send(req.params);
});

app.post("/", async (req, res) => {
  res.send("POST request to the homepage");
});

app.get("/photos", async (req, res) => {
  try {
    const files = await getUploadedFiles();
    const fileData = files.map((file) => ({ src: `/uploads/${file}` }));
    res.json(fileData);
  } catch (err) {
    res.status(500).json({ msg: err.msg });
  }
});

app.post("/uploads", (req, res) => {
  uploader(req, res, (err) => {
    if (err) {
      res.status(400).json({ msg: "Unable to upload file!" });
    } else {
      res.status(200).json({
        photo: req.file.path,
      });
    }
  });
});

app.get("/photos/:photoId", async (req, res) => {
  try {
    const { size } = await findUploadedFile(req.params.photoId);
    console.log(size);
    res.json({ size, src: req.params.photoId });
  } catch (err) {
    res.status(404).json({ msg: "Unable to find file" });
  }
});

app.listen(port, () => {
  console.log(`Server is now running on port ${port}`);
});
