import multer from "multer";
import path from "path";
import { v4 } from "uuid";
import fs from "fs/promises";

export const uploadDirectory = "./uploads";

const checkIfDirExists = async () => {
  try {
    const dirExsits = await fs.stat();
    cb(null, uploadDirectory);
  } catch (err) {
    if (err.code === "ENOENT") {
      await fs.mkdir(uploadDirectory);
    }
  }
};

const storage = multer.diskStorage({
  async destination(req, file, cb) {
    try {
      await checkIfDirExists();
      cb(null, uploadDirectory);
    } catch (err) {
      cb(err);
    }
  },

  async filename(req, file, cb) {
    const fileExtension = path.extname(file.originalname);
    const fileName = `${v4()}${fileExtension}`;
    cb(null, fileName);
  },
});

function checkFileType(file, cb) {
  const fileTypes = /jpeg|jpg|png|gif/;
  const extName = fileTypes.test(path.extname(file.originalname).toLowerCase());
  const mimeType = fileTypes.test(file.mimetype);
  if (mimeType && extName) {
    return cb(null, true);
  } else {
    cb("Error: Images Only!" + mimeType + extName);
  }
}

export const uploader = multer({
  storage,
  limits: { fileSize: 2097152 },
  fileFilter: function (req, file, cb) {
    checkFileType(file, cb);
  },
}).single("photo");

export const getUploadedFiles = async () => {
  return await fs.readdir(uploadDirectory);
};

export const findUploadedFile = async (fileName) => {
  const info = await fs.stat(path.resolve(uploadDirectory, fileName));
  return info;
};
